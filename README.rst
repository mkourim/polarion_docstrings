polarion-docstrings
===================

.. image:: https://gitlab.com/mkourim/polarion_docstrings/badges/master/pipeline.svg
    :target: https://gitlab.com/mkourim/polarion_docstrings/commits/master
    :alt: Pipeline status

.. image:: https://gitlab.com/mkourim/polarion_docstrings/badges/master/coverage.svg
    :target: https://gitlab.com/mkourim/polarion_docstrings/commits/master
    :alt: Coverage report

.. image:: https://img.shields.io/pypi/v/polarion-docstrings.svg
    :target: https://pypi.python.org/pypi/polarion-docstrings
    :alt: Version

.. image:: https://img.shields.io/pypi/pyversions/polarion-docstrings.svg
    :target: https://pypi.python.org/pypi/polarion-docstrings
    :alt: Supported Python versions

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
    :target: https://github.com/ambv/black
    :alt: Code style: black

Description
-----------
Library for reading Polarion docstrings used in Insights and CFME QE. Includes a flake8 extension for validating the docstrings.

Install
-------
To install the package to your virtualenv, run

.. code-block::

    pip install polarion-docstrings

or install it from cloned directory

.. code-block::

    pip install -e .

Package on PyPI <https://pypi.python.org/pypi/polarion-docstrings>
